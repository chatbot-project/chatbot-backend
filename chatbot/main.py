import os
from importlib import metadata

from dotenv import load_dotenv
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from .db import connect_to_db
from .routers import chat

app = FastAPI()

# load environment
load_dotenv(os.environ["ENV"])

# connect to the database
connect_to_db()

# Enable CORS requests
origins = []

if os.environ["ENVIRONMENT"] == "development":
    origins = os.environ["ORIGINS"].split(",")

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/version/")
async def root():
    """Display the current version"""
    version = metadata.version("chatbot")
    return {"version": version}


app.include_router(chat.router)
