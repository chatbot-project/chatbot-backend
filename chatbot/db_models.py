from mongoengine import StringField, ListField, FloatField, Document

class SessionData(Document):
    session_id: StringField = StringField(unique=True, required=True)
    chatbot_messages: ListField = ListField(default=[])
    human_messages: ListField = ListField(default=[])
    timestamp: FloatField = FloatField(required=True)
