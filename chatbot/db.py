from mongoengine import connect
import os

def connect_to_db():
    """Connect to the db"""

    connect(alias="default", host=os.environ["DB_HOST"])
