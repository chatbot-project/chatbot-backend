import os
import subprocess
from pathlib import Path

from fastapi import APIRouter, HTTPException, status
from mongoengine.errors import NotUniqueError, ValidationError

from ..db_models import SessionData as SessionDataDBModel
from ..models import SessionData

router = APIRouter(prefix="/chat")


async def get_hello():
    return {"message": "Hello World"}


@router.get("/red_apples/", tags=["chat", "chatbot"])
async def get_the_number_of_red_apples(num: int):

    if num < 0 or num > 3:
        raise HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            detail="The `num` query parameter must be between 0 and 3.",
        )

    optimal_speaker_model_path = os.path.join(
        Path(__file__).parents[2], "webppl", "rsa-optimal-speaker.wppl"
    )

    webppl_program_output: subprocess.CompletedProcess = subprocess.run(
        [
            "webppl",
            optimal_speaker_model_path,
            f"{num}",
        ],
        capture_output=True,
    )

    if webppl_program_output.returncode != 0:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail={"stderr": webppl_program_output.stderr.decode("utf-8")},
        )

    output_string: str = webppl_program_output.stdout.decode("utf-8").split("\n")[0]

    utterance: str = output_string.split(",")[0]
    prob: float = float(output_string.split(",")[1])

    return {"utterance": utterance, "probability": prob}


@router.post("/human-response/", tags=["chat", "human"])
async def human_response(session_data: SessionData):

    session_data_dict: dict = dict(session_data)
    try:
        new_session_data: SessionDataDBModel = SessionDataDBModel(**dict(session_data))
        new_session_data.save()
    except NotUniqueError:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail=f'Session id "{session_data_dict["session_id"]}" already exists.',
        )
    except ValidationError as exc:
        raise HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, detail=str(exc)
        )

    print(dict(session_data))
    return {"status": "ok"}
