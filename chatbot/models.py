from pydantic import BaseModel

class SessionData(BaseModel):
    session_id: str
    chatbot_messages: list
    human_messages: list
    timestamp: float
