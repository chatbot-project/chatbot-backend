/*
SETUP THE DEVELOPMENT DB
*/

connection = new Mongo();

db.connection.getDB('session_data');

// Remove any session_data from the database
// that may have been stayed there from a previous
// development session.
db.session_data.deleteMany({})

// Add a user to the db
db.createUser({
  user: "dev-user",
  pwd: "test",
  roles: [
    { role: "readWrite", db: "session_data" },
  ],
});
