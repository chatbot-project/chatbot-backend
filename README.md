# MVP for a Chatbot Based on Scalar Implicature

## Features

- Api: receive a message about how many apples are red ('all', 'some', 'none')
- Chatbot-listener: make an inference about how many apples are red
- chatbot-speaker: get info about how many apples are red, and respond to it
- store responses from the user

## Usage

### Setup

Run the app:

```sh
poetry run env ENV=example.env uvicorn chatbot.main:app --host=0.0.0.0 --reload
```

- Run the db

```sh
docker-compose --profile development up -d
```

- Connect to the db from the terminal:

```sh
mongosh -u dev-user -p test --authenticationDatabase session_data localhost:27017/session_data
```

### Requests

Use [httpie](https://httpie.io/cli) for the requests.

- Get the latest version:

```sh
http get 'localhost:8000/version/'
```

- Get the most optimal utterance for 0 red apples according to the RSA Scalar Implicature Model:

```sh
http get 'localhost:8000/chat/red_apples/?num=0'
```

- Post chat history for a new session:

```sh
echo '{"session_id": 1234, "chatbot_messages":[], "human_messages":[], "timestamp":1}' | http post 'localhost:8000/chat/human-response/'
```

- List all session_data from the db:

```sh
mongosh -u dev-user -p test --authenticationDatabase session_data localhost:27019/session_data --eval 'db.session_data.find({})'
```

- Delete all the documents from the `session_data` collection (useful for testing):

```sh
mongosh -u dev-user -p test --authenticationDatabase session_data localhost:27019/session_data --eval 'db.session_data.deleteMany({})'
```

## Tests

Run end-to-end-tests:

> Don't forget to run the server on `localhost:8000`!

```sh
hurl hurl/*.hurl --test
```
